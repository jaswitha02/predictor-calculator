<?php
namespace Drupal\drupal_simple_form\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Our simple form class.
 */
class SimpleForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_simple_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $genderOptions = array(
      '0' => 'Select Gender',
      'Male' => 'Male',
      'Female' => 'Female',
      'Other' => 'Other'

);

  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('First Name:'),
    //'#required' => TRUE,
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Last Name:'),
    //'#required' => TRUE,
  );
  $form['email'] = array(
    '#type' => 'email',
    '#title' => $this->t('Email ID:'),
    //'#required' => TRUE,
  );
  $form['candidate_number'] = array (
    '#type' => 'tel',
    '#title' => $this->t('Mobile no'),
  );
  $form['candidate_dob'] = array (
    '#type' => 'date',
    '#title' =>$this->t('DOB'),
    //'#required' => TRUE,
  );
  
  $form['gender'] = array(
    '#type' => 'select',
    '#title' => 'Gender',
    '#options' =>$genderOptions 
  );
  $form['candidate_confirmation'] = array (
    '#type' => 'radios',
    '#title' => ('Are you above 18 years old?'),
    '#options' => array(
      'Yes' =>$this->t('Yes'),
      'No' =>$this->t('No')
    ),
  );
  $form['Address1'] = array (
    '#type' => 'textfield',
    '#title' =>$this->t('Address Line1'),
    //'#required' => TRUE,
  );
  $form['Address2'] = array(
    '#type' => 'textfield',
      '#title' => $this->t('Address Line2'),
      //'#required' => TRUE
       );
       $form['country'] = array(
        '#type' => 'textfield',
          '#title' => $this->t('country'),
          //'#required' => TRUE
           );
           
  $form['Address'] = array (
    '#type' => 'textfield',
    '#title' => $this->t('State'),
   // '#required' => TRUE,
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $this->t('Submit'),
    '#button_type' => 'primary',
  );
    return $form;
  }
 
  /**
   * {@inheritdoc}
   */

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $first_name = $form_state->getValue('first_name');
    if (strlen($form_state->getValue('first_name')) == '') {
      $form_state->setErrorByName('first_name', $this->t('first name field is can not empty.'));
    }
     elseif(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('first_name'))){
      $form_state->setErrorByName('first_name', $this->t('first name should not contain special Characters'));
    }

    if(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('last_name'))){
      $form_state->setErrorByName('last_name', $this->t('last name should not contain special Characters'));
    }
    
     $candidate_number= $form_state->getValue('candidate_number');
     if (strlen($form_state->getValue('candidate_number')) == '') {
      $form_state->setErrorByName('candidate_number', $this->t('Mobile number field is can not empty.'));
    }
    elseif(strlen($candidate_number) > 0 && !preg_match('/^[1-9][0-9]*$/', $candidate_number))
    {
    $form_state->setErrorByName('candidate_number', $this->t('Mobile number should not contain characters'));
    }

     //$candidate_number= $form_state->getValue('candidate_number');
    //elseif(strlen($candidate_number) > 0  && !preg_match('/^[1-9][0-9]*$/', $candidate_number)){
    //$form_state->setErrorByName('candidate_number', $this->t('Mobile number should not contain characters'));
    
    //}
   

     if($form_state->getValue('gender') == '0'){
      $form_state->setErrorByName('gender', $this->t('Gender field is required'));
    }

    
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('@your_name ,Your application is being submitted!', array('@your_name' => $form_state->getValue('your_name'))));
     foreach ($form_state->getValues() as $key => $value) {
       drupal_set_message($key . ': ' . $value);
       $postData = $form_state->getValue();
       echo "<pre>";
       print_r($postData);
       echo "</pre>";
       exit;
     }
    }
  }