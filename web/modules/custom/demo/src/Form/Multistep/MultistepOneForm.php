<?php
/**
 * @file
 * Contains \Drupal\demo\Form\Multistep\MultistepOneForm.
 */

namespace Drupal\demo\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;

class MultistepOneForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_one';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#default_value' => $this->store->get('name') ? $this->store->get('name') : '',
    );

    $form['email'] = array(
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#default_value' => $this->store->get('email') ? $this->store->get('email') : '',
    );

    
    $form['actions']['submit']['#value'] = $this->t('Next');
    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $first_name = $form_state->getValue('name');
    if (strlen($form_state->getValue('name')) == '') {
      $form_state->setErrorByName('name', $this->t('name field is can not empty.'));
    }
     elseif(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('name'))){
      $form_state->setErrorByName('name', $this->t('name should not contain special Characters'));
     }
    }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('email', $form_state->getValue('email'));
    $this->store->set('name', $form_state->getValue('name'));
    $form_state->setRedirect('demo.multistep_two');
  }
}