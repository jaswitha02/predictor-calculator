<?php
/**
 * @file
 * Contains Drupal\exampleform\Form\MultiStepForm.
 */
namespace Drupal\exampleform\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
class MultiStepForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
  }
  /**
   * {@inheritdoc}
   */
  public function getFormID()
  {
    return 'multi_step_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($form_state->has('page') && $form_state->get('page') == 2) {
      return self::formPageTwo($form, $form_state);
    }
    $form_state->set('page', 1);
    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Page @page',['@page'=>$form_state->get('page')]),
    ];
    $form['Your_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Name'),
      '#default_value' => $form_state->getValue('Your_name', ''),
      //'#required' => TRUE,
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email ID'),
      '#default_value' => $form_state->getValue('email', ''),
     // '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      '#submit' => ['::submitPageOne'],
      '#validate' => ['::validatePageOne'],
    ];
    return $form;
  }
  public function validatePageOne(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('Your_name');
    if (strlen($title) < 5) {
      $form_state->setErrorByName('Your_name', $this->t('The Your name must be at least 5 characters long.'));
    }
    if(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('Your_name')))
  {
    $form_state->setErrorByName('Your_name', $this->t('Your name should not contain special characters'));
   }
   //email validation
   $email = $form_state->getvalue('email');
   if(trim($email) == ''){
     $form_state->setErrorByName('email', $this->t('please enter the email'));
   }
   if(!preg_match("/^([a-z0-9\+_\-]+)(\. [a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[ a-z]{2,6}$/ix",$form_state->getvalue('email')))
   {
     $form_state->setErrorByName('email',$this->t('Invalid email'));
   }
   //ending emailvalidation
  }
  /**
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
public function submitPageOne(array &$form, FormStateInterface $form_state) {
  $form_state
    ->set('page_values', [
      'Your_name' => $form_state->getValue('Your_name'),
      'email' => $form_state->getValue('email'),
    ])
    ->set('page', 2)
    ->setRebuild(TRUE);
}
/**
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 * @return array
 *   The render array defining the elements of the form.
 */
public function formPageTwo(array &$form, FormStateInterface $form_state) {
  $form['description'] = [
    '#type' => 'item',
    '#title' => $this->t('Page @page',['@page'=>$form_state->get('page')]),
  ];
  $form['age'] = [
    '#type' => 'number',
    '#title' => $this->t('Your age'),
    '#required' => TRUE,
    '#default_value' => $form_state->getValue('age', ''),
  ];
  $form['Location'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Your Location'),
    '#required' => TRUE,
    '#default_value' => $form_state->getValue('Location', ''),
  ];
  $form['back'] = [
    '#type' => 'submit',
    '#value' => $this->t('Back'),
    '#submit' => ['::pageTwoBack'],
    '#limit_validation_errors' => [],
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#button_type' => 'primary',
    '#value' => $this->t('Submit'),
  ];
  return $form;
}
/**
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
public function pageTwoBack(array &$form, FormStateInterface $form_state) {
  $form_state
    ->setValues($form_state->get('page_values'))
    ->set('page', 1)
    ->setRebuild(TRUE);
}
/**
 * @param array $form
 * @param FormStateInterface $form_state
 */
public function validateForm(array &$form, FormStateInterface $form_state)
{
  // validate form
}
/**
 * @param array $form
 * @param FormStateInterface $form_state
 */
public function submitForm(array &$form, FormStateInterface $form_state)
{
  //Drupal::messenger()->addMessage('Thank you');
  //drupal_set_message($this->t('@your_name ,Your application is being submitted!', array('@your_name' => $form_state->getValue('your_name'))));
  foreach ($form_state->getValues() as $key => $value) {
    drupal_set_message($key . ': ' . $value);
    $postData = $form_state->getValue();
    echo "<pre>";
    print_r($postData);
    echo "</pre>";
    exit;
  }
}
}
?>